$(document).ready(function(){
    setTimeout(function(){ $(".wrapper").fadeIn();  }, 500);
})

    $(function(){
        $("#wizard").steps({
            headerTag: "h4",
            bodyTag: "section",
            transitionEffect: "fade",
            enableAllSteps: true,
            onStepChanging: function (event, currentIndex, newIndex) { 
                console.log(currentIndex,newIndex);
                if ( newIndex === 1 ) {
                    $('.wizard > .steps ul').addClass('step-2');
                } else {
                    $('.wizard > .steps ul').removeClass('step-2');
                }
                if ( newIndex === 2 ) {
                    $('.wizard > .steps ul').addClass('step-3');
                } else {
                    $('.wizard > .steps ul').removeClass('step-3');
                }
                if ( newIndex === 3 ) {
                    $('.wizard > .steps ul').addClass('step-4');
                } else {
                    $('.wizard > .steps ul').removeClass('step-4');
                }
                if ( newIndex === 4 ) {
                    $('.wizard > .steps ul').addClass('step-5');
                } else {
                    $('.wizard > .steps ul').removeClass('step-5');
                }
                
                if ( newIndex === 5 ) {
                    $('.wizard > .steps ul').addClass('step-6');
                } else {
                    $('.wizard > .steps ul').removeClass('step-6');
                }
                if ( newIndex === 6 ) {
                    $('.wizard > .steps ul').addClass('step-7');
                } else {
                    $('.wizard > .steps ul').removeClass('step-7');
                }
                if ( newIndex === 7 ) {
                    $('.wizard > .steps ul').addClass('step-8');
                } else {
                    $('.wizard > .steps ul').removeClass('step-8');
                }
                if ( newIndex === 8 ) {
                    $('.wizard > .steps ul').addClass('step-9');
                } else {
                    $('.wizard > .steps ul').removeClass('step-9');
                }
                if ( newIndex === 9 ) {
                    $('.wizard > .steps ul').addClass('step-10');
                } else {
                    $('.wizard > .steps ul').removeClass('step-10');
                }
                if ( newIndex === 10 ) {
                    $('.wizard > .steps ul').addClass('step-11');
                } else {
                    $('.wizard > .steps ul').removeClass('step-11');
                }
                return true; 
            },
            labels: {
                finish: "Enviar",
                next: "Siguiente",
                previous: "Anterior"
            }
        });
        // Custom Button Jquery Steps
        $('.forward').click(function(){
            $("#wizard").steps('next');
        })
        $('.backward').click(function(){
            $("#wizard").steps('previous');
        })
        // Date Picker
        var dp1 = $('#dp1').datepicker().data('datepicker');
        dp1.selectDate(new Date());
    })

$(document).ready(function(){
    $(".multiple").click(function(){
        if($(this).hasClass("selected")){
            $(this).removeClass("selected");
            $(this).prop( "checked",false);
        }else{
            $(this).addClass("selected");
            $(this).prop( "checked",true);
        }
    })

    $(".unique").click(function(){
        $(this).attr("field")
        if($(this).hasClass("selected")){
            $(this).removeClass("selected");
            $(this).prop( "checked",false);
        }else{
            $("."+$(this).attr("field")).removeClass("selected");
            $("."+$(this).attr("field")).prop( "checked",false);
            $(this).addClass("selected");
            $(this).prop( "checked",true);
        }
    })


    // Masked Input
(function( $ ) {

	'use strict';

	if ( $.isFunction($.fn[ 'mask' ]) ) {

		$(function() {
			$('[data-plugin-masked-input]').each(function() {
				var $this = $( this ),
					opts = {};

				var pluginOptions = $this.data('plugin-options');
				if (pluginOptions)
					opts = pluginOptions;

				$this.themePluginMaskedInput(opts);
			});
		});

	}

}).apply(this, [ jQuery ]);
/*$(document).ready(function(){
    $(".goals").click(function(){
        if($(this).hasClass("selected")){


			$(this).removeClass("selected");
            $(this).prop( "checked",false);
        }else{
            $(".goals").removeClass("selected");
            $(".goals").prop( "checked",false);
            $(this).addClass("selected");
            $(this).prop( "checked",true);
        }
    })
})
*/
})




    function summit(){
        var selections = [];
        $(".selected").each(function(){
            if( selections[$(this).attr("field")] === undefined){
                selections[$(this).attr("field")] = $(this).val();
            }else{
                selections[$(this).attr("field")] = selections[$(this).attr("field")] +','+$(this).val();
            }
        })
        console.log(selections);
    }



	
function IsNumeric(valor) 
{ 
var log=valor.length; var sw="S"; 
for (x=0; x<log; x++) 
{ v1=valor.substr(x,1); 
v2 = parseInt(v1); 
//Compruebo si es un valor numérico 
if (isNaN(v2)) { sw= "N";} 
} 
if (sw=="S") {return true;} else {return false; } 
} 
var primerslap=false; 
var segundoslap=false; 
function formateafecha(fecha) 
{ 
var long = fecha.length; 
var dia; 
var mes; 
var ano; 
if ((long>=2) && (primerslap==false)) { dia=fecha.substr(0,2); 
if ((IsNumeric(dia)==true) && (dia<=31) && (dia!="00")) { fecha=fecha.substr(0,2)+"/"+fecha.substr(3,7); primerslap=true; } 
else { fecha=""; primerslap=false;} 
} 
else 
{ dia=fecha.substr(0,1); 
if (IsNumeric(dia)==false) 
{fecha="";} 
if ((long<=2) && (primerslap=true)) {fecha=fecha.substr(0,1); primerslap=false; } 
} 
if ((long>=5) && (segundoslap==false)) 
{ mes=fecha.substr(3,2); 
if ((IsNumeric(mes)==true) &&(mes<=12) && (mes!="00")) { fecha=fecha.substr(0,5)+"/"+fecha.substr(6,4); segundoslap=true; } 
else { fecha=fecha.substr(0,3);; segundoslap=false;} 
} 
else { if ((long<=5) && (segundoslap=true)) { fecha=fecha.substr(0,4); segundoslap=false; } } 
if (long>=7) 
{ ano=fecha.substr(6,4); 
if (IsNumeric(ano)==false) { fecha=fecha.substr(0,6); } 
else { if (long==10){ if ((ano==0) || (ano<1900) || (ano>2009)) { fecha=fecha.substr(0,6); } } } 
} 
if (long>=10) 
{ 
fecha=fecha.substr(0,10); 
dia=fecha.substr(0,2); 
mes=fecha.substr(3,2); 
ano=fecha.substr(6,4); 
// Año no viciesto y es febrero y el dia es mayor a 28 
if ( (ano%4 != 0) && (mes ==02) && (dia > 28) ) { fecha=fecha.substr(0,2)+"/"; } 
} 
return (fecha); 
}
